<!doctype html>
<html lang="fr">
<head>
	<meta charset="utf-8">
	<title>Administration rencontre ligue de basket</title>
</head>
<body>
	<h1>Panel d'administration de la rencontre annuelle de la ligue de basket</h1>
	<h2>Liste des inscrits</h2>
<?php
	//Spécifie la racine du dossier pour éviter les erreurs de chemin
	define('ROOT', '..');
	require ROOT.'/user.php';

	if(!empty($_GET['del']))
	{
		$ok = user_delete($_GET['del']);
		if($ok)
			echo "Utilisateur supprimé avec succès.<br>";
		else
			echo "Erreur lors de la suppression de l'utilisateur.<br>";
	}

	$users = user_get_all();
	if($users)
	{
		//S'il y a des utilisateurs inscrits
		if(count($users['users']) > 0)
		{
			//Nous construisons un tableau HTML contenant toutes les informations des utilsateurs
			//Ainsi qu'un lien de suppression
			echo "<table><tr><th>Nom</th><th>Prénom</th><th>Club</th><th>Suppression</th></tr>";
			foreach ($users['users'] as $key => $usr) {
				echo "<tr>";
					echo "<td>".$usr['lastname']."</td><td>".$usr['firstname']."</td><td>".$usr['club']."</td>";
					//Le lien de suppression est tout simplement l'adresse du panel admin
					//Avec un paramètre GET en plus qu'on nomme "del" et auxquel on donne
					//L'id d'utilisateur comme valeure
					echo "<td><a href='index.php?del=".$usr['id']."'>Supprimer l'utilisateur</a></td>";
				echo "</tr>";
			}
			echo "</table>";
		}
		else
			echo "Aucun utilisateurs inscrits pour le moment.";
	}
	else
		echo "Il y a eu une erreur lors de la récupération des utilisateurs inscrits";
?>
</body>
</html>