<?php
define('JSON_FILE', ROOT.'/data/users.json');
/*
 * Cette fonction vérifie les données d'inscription de l'utilisateur
 * Et renvoie un tableau contenant les éventuelles erreurs s'il y en a
 * ainsi que les données de l'utilisateur
 */
function user_check_data($data)
{
	$res = ['errors' => [], 'user' => []];
	//Pour ne pas avoir à faire ce que l'on appelle une 
	//"foret de if" (qui est un nom d'arbre d'ailleurs)
	//J'utilise un tableau contenant tous les champs que je souhaite vérifier
	$fields = ['firstname' => 'Prénom',
				'lastname' => 'Nom',
				'birthday' => 'Date de naissance',
				'num_people' => 'Nombre de personnes accompagnantes',
				'club' => 'Club',
				];
	//Et je boucle dessus pour voir si le champs n'est pas vide
	//Note : on aurait pu pousser la validation avec des expression régulières
	//Ou autre mais ça aurait complexifié le traitement pour l'exercice
	foreach ($fields as $key => $value) {
		//empty regarde si la variable existe et qu'elle n'est pas vide
		//Le chiffre 0 est considéré comme vide donc si on a pas de personnes accompagnates
		//Ca affichera une erreur
		//On va donc traiter ce cas particulier on s'assurant que la valeure est juste renseignée
		if($key !== 'num_people' && empty($data[$key])
			|| ($key === 'num_people' && !isset($data[$key])))
		{
			//Noter le []= qui permet de rajouter un élément à un tableau
			$res['errors'] []= "Le champs ".$value." est vide.";
		}

		//On remplit l'utilisateur avec les données envoyées
		$res['user'][$key] = $data[$key];
	}

	if(user_exists($data))
		$res['errors'] []= "Cet utilisateur existe déjà, vous ne pouvez pas vous inscrire deux fois à l'évènement";

	return $res;
}

/*
 * Renvoi tous les utilisateurs contenu dans le fichier JSON
 * Ou un tableau vide si pas de fichier
 * Et false s'il y a eu une erreur
 * Note : créé le fichier JSON s'il n'existe pas
 */
function user_get_all()
{
	if(!file_exists(JSON_FILE))
		return ['last_id' => 0, 'users' => []];
	//Noter que je passe true au deuxième argument afin que mon résultat soit convertit
	//en tableau associatif PHP
	$json_users = file_get_contents(JSON_FILE);
	if(!$json_users)
		return false;
	return json_decode($json_users, true);
}

/*
 * Sauvegarde le tableau d'utilisateurs dans un fichier JSON
 */
function user_save_all($users)
{
	$json_users = json_encode($users, JSON_PRETTY_PRINT);
	if(!$json_users)
		return false;
	return file_put_contents(JSON_FILE, $json_users);
}

/*
 * Inscrit l'utilisateur à l'événement et lui assigne un ID
 */
function user_sign_up($data)
{
	$users = user_get_all();
	if(!$users)
		return false;

	//On rajoute un id à l'utilisateur en se basant sur le dernier id donné
	$users['last_id'] = $users['last_id'] + 1;
	$data['id'] = $users['last_id'];
	$users['users'] []= $data;

	user_save_all($users);
}


/*
 * Renvoi true si l'utilisateur passé en paramètre existe déjà
 * false dans les autres cas
 */
function user_exists($user)
{
	$users = user_get_all();
	if($users)
	{
		foreach ($users['users'] as $key => $usr) {
			if($usr['firstname'] == $user['firstname'] 
				&& $usr['lastname'] == $user['lastname'] 
				&& $usr['birthday'] == $user['birthday'])
				return true;
		}
	}
	return false;
}

/*
 * Supprime l'utilisateur dont l'id est passé en paramètre
 * Renvoi true si l'utilisateur a bien été supprimé
 * false s'il n'a pas été trouvé ou en cas d'erreur
 */
function user_delete($user_id)
{
	$users = user_get_all();
	if($users)
	{
		foreach ($users['users'] as $key => $usr) {
			if($usr['id'] == $user_id)
			{
				array_splice($users['users'], $key, 1);
				return user_save_all($users);
			}
		}
	}
	return false;
}