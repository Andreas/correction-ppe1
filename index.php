<!doctype html>
<html lang="fr">
<head>
	<meta charset="utf-8">
	<link rel="stylesheet" href="ppe.css">
	<title>Inscription rencontre ligue de basket</title>
</head>
<body>
	<h1>Inscription à la rencontre annuelle de la ligue de basket</h1>
	<?php
		$display_form = true;
		//On ne traite l'inscription que lorsque l'utilisateur à cliqué sur le bouton
		//Ce qui génère une requète POST avec les valeurs du formulaires dedans
		if(isset($_POST['signup']))
		{
			define('ROOT', '.');
			//Le code de gestion de l'inscription est dans un fichier PHP à part
			//Afin de bien séparer la logique applicative de l'interface HTML
			require 'user.php';
			//Je passe la variable $_POST à ma fonction comme ça
			//Elle est indépendante de $_POST. Ainsi, je pourrai
			//Aussi bien l'utiliser avec un tableau de donner provenant
			//D'un CSV par exemple pour faire de l'inscription de masse.
			$res = user_check_data($_POST);
			//Si le formulaire ne comporte pas d'erreur
			if(count($res['errors']) === 0)
			{
				//On peut inscrire l'utilisateur
				$ok = user_sign_up($res['user']);
				if($ok !== false)
				{
					echo "Votre inscription a bien été prise en compte.";
					$display_form = false;
				}
				//Il y a eu un problème interne non lié à l'utilisateur
				//Nous n'affichons pas d'erreur précise
				else
				{
					echo "Une erreur est survenue lors de votre inscription. Merci de réessayer plus tard.";
				}
			}
			else
			{
				//Sinon on affiche les erreurs
				echo "Votre formulaire comporte des erreurs, merci de les corriger :<br><ul>";
				foreach ($res['errors'] as $key => $error) {
					echo '<li>'.$error.'</li>';
				}
				echo "</ul>";
			}
		}

		if($display_form)
		{
	?>
	<form method="post">
		<div>
			<label for="sex">Civilité * </label>
			<input required type="radio" name="sex" value="male" class="radio">Mr</input>
			<input required type="radio" name="sex" value="female" class="radio">Mme</input>
		</div>
		<div>
			<label for="firstname">Prénom * </label>
			<input required type="text" name="firstname">
		</div>
		<div>
			<label for="lastname">Nom * </label>
			<input required type="text" name="lastname">
		</div>
		<div>
			<label for="birthday">Date de naissance * </label>
			<input required type="date" name="birthday">
		</div>
		<div>
			<label for="num_people">Nb. personnes accompagnantes * </label>
			<input required type="number" name="num_people" min='0' max="15">
		</div>
		<div>
			<label for="club">Club * </label>
			<select required name="club" placeholder="Selectionnez votre club">
				<option value="" disabled selected>Selectionnez votre club</option>
				<option value="Lyon">Lyon</option>
				<option value="Villeurbanne">Villeurbanne</option>
				<option value="Bron">Bron</option>
				<option value="Oulins">Oulins</option>
				<option value="St Priest">St Priest</option>
				<option value="Pierre Bénite">Pierre Bénite</option>
				<option value="Vaulx en Velin">Vaulx en Velin</option>
			</select>
		</div>
		<div>
			<label for="food">Boissons ou nourriture</label>
			<textarea name="food"></textarea>
		</div>
		(*) Champs requis
		<button type="submit" name="signup" class="button">Valider mon inscription</button>

	</form>
	<?php
		}
	?>
</body>
</html>