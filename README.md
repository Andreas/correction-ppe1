# Correction du PPE 1 de BTS SIO, Infosup Lyon 2015-2016

Voici une proposition de correction pour le PPE 1 dont le sujet se trouve dans le projet.

J'ai essayé de faire quelque chose de simple qui n'utilise pas des fonctionnalités avancées de PHP tout en tirant parti des derniers standards (les formulaires HTML 5 notament).

J'ai choisi d'utiliser un fichier JSON pour la sauvegarde des données pour vous montrer comment cela peut-être fait de cette manière et parce que je trouve cela plus simple que de mettre en place une base de données MySQL.

Bien entendu, cela n'est pas valable pour un vrai projet et il est vivement recommandé de stocker ses données en utilisant une base de données.

Enfin, l'authentification se fait via fichier .htaccess, pour que cela fonctionne, il faudra revoir les chemins en fonction de l'installation de votre serveur. Sinon, la partie admin du site sera accessible à tout le monde.

## Critiques et évolutions

Le code du projet est loin d'être exemplaire et les sites internet ne se font guère plus comme cela de nos jours.

Voici quelques pistes d'évolutions :
* Mettre les fonctions de user.php dans une classe (Programmation Orientée Objet)
* Développé un mini framework MVC pour séparer d'avantage la logique métier de l'affichage
* Utiliser Twitter bootstrap pour que ça soit bôôo
* Faire des requètes AJAX pour que ça soit dynamiqueuuuu
* Faire les autres bonus

Si vous êtes intéressé pour aller plus loin, n'hésitez pas à me contacter par mail.

Andréas